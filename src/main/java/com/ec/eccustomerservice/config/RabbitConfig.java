package com.ec.eccustomerservice.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue createQueue() {
        return new Queue("ec_customer_product_queue", true);
    }

}
