package com.ec.eccustomerservice.model;

import com.ec.eccustomerservice.validation.Validator;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Customer {

    @Id
    @NotEmpty(groups = Validator.class)
    @NotNull(groups = Validator.class)
    private String id;

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    @NotEmpty(message = "Email must be filled", groups = Validator.class)
    private String email;

    @Column
    private String phone;

    @Column
    private LocalDateTime timeStamp = LocalDateTime.now();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
    @Valid
    private Set<Product> products = new HashSet();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
        addProduct();
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void addProduct(){
        this.products.forEach(p -> {
            p.setCustomer(this);
        });
    }
}
