package com.ec.eccustomerservice.service;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MessageListener {

    private final CustomerService customerService;

    @Autowired
    public MessageListener(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "ec_customer_product_queue", durable = "true"),
            exchange = @Exchange(value = "auto.exch"),
            key = "orderRoutingKey")
    )
    public void processOrder(byte[] data) throws IOException {
        customerService.createCustomerModel(new String(data));
    }

}