package com.ec.eccustomerservice.service;

import com.ec.eccustomerservice.model.Customer;
import com.ec.eccustomerservice.repository.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final ObjectMapper objectMapper;

    public CustomerService(CustomerRepository customerRepository, ObjectMapper objectMapper) {
        this.customerRepository = customerRepository;
        this.objectMapper = objectMapper;
    }

    public Customer getCustomerById(String customerId){
        return this.customerRepository.findById(customerId).orElse(null);
    }

    public Customer saveCustomer(Customer customer){
        return this.customerRepository.save(customer);
    }

    void createCustomerModel(String modelData) throws IOException {
        Customer customer = objectMapper.readValue(modelData, Customer.class);
        saveCustomer(customer);
    }
}
