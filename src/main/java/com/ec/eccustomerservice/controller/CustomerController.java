package com.ec.eccustomerservice.controller;

import com.ec.eccustomerservice.model.Customer;
import com.ec.eccustomerservice.service.CustomerService;
import com.ec.eccustomerservice.validation.Validator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController("v1")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }

    @PostMapping
    public ResponseEntity<Customer> saveCustomer(@RequestBody @Validated(Validator.class) Customer customer) {
        return new ResponseEntity<>(customerService.saveCustomer(customer),HttpStatus.OK);
    }

    @GetMapping("/{customerId}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable String customerId){
        return new ResponseEntity<>(customerService.getCustomerById(customerId), HttpStatus.OK);
    }

}
