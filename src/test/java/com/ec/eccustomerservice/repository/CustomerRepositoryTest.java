package com.ec.eccustomerservice.repository;

import com.ec.eccustomerservice.model.CreateModels;
import com.ec.eccustomerservice.model.Customer;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;

    private CreateModels createModels = new CreateModels();

    private Customer customer = createModels.createCustomer();

    @Test
    public void shouldPersistCustomer() {

        customerRepository.save(customer);

        Optional<Customer> persistedCustomer = customerRepository.findById("123454323444");

        Assertions.assertNotNull(persistedCustomer.get());
        Assertions.assertEquals(persistedCustomer.get().getId(), "123454323444");
        Assertions.assertEquals(1, persistedCustomer.get().getProducts().size());
    }

    @Test
    public void shouldPersistCustomerWithoutProduct() {

        customer.setProducts(new HashSet<>());
        customerRepository.save(customer);
        Optional<Customer> persistedCustomer = customerRepository.findById("123454323444");

        Assertions.assertEquals(0, persistedCustomer.get().getProducts().size());
    }

}
