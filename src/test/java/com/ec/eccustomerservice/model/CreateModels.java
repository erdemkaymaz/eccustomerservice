package com.ec.eccustomerservice.model;

public class CreateModels {

    public Customer createCustomer(){

        Customer customer = new Customer();
        customer.setEmail("xxxx@gmail.com");
        customer.setId("123454323444");
        customer.setName("xxxx");
        customer.setSurname("xxxx");
        customer.setPhone("4324234443");

        Product product = createProduct();
        product.setCustomer(customer);

        customer.getProducts().add(product);

        return customer;
    }

    public Product createProduct() {

        Product product = new Product();
        product.setId("123456");
        product.setModel("AAAXXXBBB");
        product.setPrice(100);
        product.setType("shoe");
        return product;
    }

}
