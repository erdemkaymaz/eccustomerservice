package com.ec.eccustomerservice.validation;

import com.ec.eccustomerservice.model.CreateModels;
import com.ec.eccustomerservice.model.Customer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;


@SpringBootTest
public class ValidationTest {

    private CreateModels createModels = new CreateModels();

    private Customer customer = createModels.createCustomer();

    private javax.validation.Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldNotValidateCustomerWithNullId() {
        customer.setId(null);
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer, com.ec.eccustomerservice.validation.Validator.class);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void shouldNotValidateCustomerWithNullEmail() {
        customer.setEmail(null);
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer, com.ec.eccustomerservice.validation.Validator.class);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void shouldNotValidateCustomerWithTheProductWithNullType() {
        customer.getProducts().forEach(p -> {
            p.setType(null);
        });
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer, com.ec.eccustomerservice.validation.Validator.class);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void shouldNotValidateCustomerWithTheProductWithZeroOrNegativePrice() {
        customer.getProducts().forEach(p -> {
            p.setPrice(-1);
        });
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer, com.ec.eccustomerservice.validation.Validator.class);
        assertFalse(violations.isEmpty());
    }

}
