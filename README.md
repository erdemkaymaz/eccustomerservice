This application is being developed for dig diving into modern microservice infrastructures 
that provide redundancy and overcome heavy load with docker swarm cluster and haproxy.

- Spring Boot
- HAPROXY
- RabbitMQ
- Docker
- Docker Swarm
- Mysql
- Jira
- Confluence
- Git

![Desired Architecture](doc/design.pdf)